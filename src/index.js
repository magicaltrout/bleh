import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import AxiosKeyloak from './ax.js'

const kc = new AxiosKeyloak('./keycloak.json')
const axiosInstance = kc.createAxiosInstance()

// Make a request for a user with a given ID


//const kc = Keycloak();
const token = localStorage.getItem('KC_TOKEN');
const refreshToken = localStorage.getItem('KC_REFRESH_TOKEN');


kc.init({ onLoad: 'login-required', token, refreshToken })
    .success(authenticated => {
        if (authenticated) {
           /* axios.get('https://otv.spicule.co.uk/pentaho/Home')
                .then(function (response) {
                    // handle success
                    console.log(response);
                    axios.get('https://otv.spicule.co.uk/pentaho/api/repo/files/:public:OTV/tree?showHidden=false&filter=*.wcdf|FILES_FOLDERS', { headers: {  'content-type': 'application/json',
                            'Authorization': `Bearer `+kc.token} })
                        .then(function (response) {
                            // handle success
                            debugger;
                            console.log(response);

                            axios.get('https://otv.spicule.co.uk/pentaho/api/repo/files/:public:OTV/tree?showHidden=false&filter=*.wcdf|FILES_FOLDERS', { headers: {  'content-type': 'application/json',
                                    'Authorization': `Bearer `+kc.token} })
                                .then(function (response) {
                                    // handle success
                                    debugger;
                                    console.log(response);
                                })
                                .catch(function (error) {
                                    // handle error
                                    debugger;
                                    console.log(error);
                                })
                                .then(function () {
                                    // always executed
                                });
                        })
                        .catch(function (error) {
                            // handle error
                            debugger;
                            console.log(error);
                        })
                        .then(function () {
                            // always executed
                        });
                })
                .catch(function (error) {
                    // handle error
                    debugger;
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });*/
            ReactDOM.render(<App kc={kc} token={kc.token}/>, document.getElementById('root'));


        } else {
            console.log('Error to authenticate');
            ReactDOM.render(<App/>, document.getElementById('root'));

        }
    })
    .error(err => console.error('Failed to initialize'));

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
